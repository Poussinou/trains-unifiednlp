/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains;

import android.location.Location;
import android.util.Log;
import java.util.function.Consumer;

public abstract class IntervalProvider extends Provider {
	public IntervalProvider(Consumer<Location> report){
		super(report);
	}

	final protected int delay = 10000;

	protected boolean autodetectBadGps = false;
	private int sameLocationCounter = 0;

	protected Thread regular;

	protected void setup() { /* do nothing here */ }

	abstract protected void callback();

	@Override
	protected void report(Location l) {
		if (l != null && autodetectBadGps) {
			// Some wifis sadly do not report a GPS status. So, we try to
			// approximate that by determening if the location is the same
			// over multiple requests
			if (lastValidLocation != null && lastValidLocation.distanceTo(l) < 0.0001f) {
				sameLocationCounter++;
			} else {
				sameLocationCounter = 0;
			}
			if (sameLocationCounter > 2) {
				l = null;
			}
		}
		super.report(l);
	}

	@Override
	public void start() {
		regular = new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (regular) {
					setup();
					while (!regular.isInterrupted()) {
						callback();
						try {
							regular.wait(delay);
						} catch (InterruptedException e) {
							return;
						}
					}
				}
			}
		});
		regular.start();
	}

	@Override
	public void stop() {
		Log.d(IntervalProvider.class.getName(), "Stopping provider...");
		if (regular != null && regular.isAlive()) {
			regular.interrupt();
		}
	}
}
