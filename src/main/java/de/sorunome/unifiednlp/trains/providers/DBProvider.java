/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains;

import android.location.Location;
import android.util.Log;
import java.net.UnknownHostException;
import java.util.function.Consumer;
import org.json.JSONException;
import org.json.JSONObject;
import org.microg.nlp.api.LocationHelper;

public class DBProvider extends IntervalProvider {
	public DBProvider(Consumer<Location> report){
		super(report);
	}

	private static final String TAG = DBProvider.class.getName();

	@Override
	protected void setup() {
		Log.d(TAG, "Got Deutsche Bahn wifi");
	}

	@Override
	protected void callback() {
		String responseString = null;
		try {
			responseString = Utils.getUrl("https://iceportal.de/api1/rs/status");
			JSONObject response = new JSONObject(responseString);
			if (!response.getString("gpsStatus").equals("VALID")) {
				report(null);
				return;
			}
			double lat = response.getDouble("latitude");
			double lng = response.getDouble("longitude");
			Location location = LocationHelper.create("trains", lat, lng, 50.0f);
			if (!response.isNull("speed")) {
				location.setSpeed((float) (response.getDouble("speed") * 10.0 / 36.0));
			}
			Log.d(TAG, "Just reported: " + location);
			report(location);
		} catch (JSONException e) {
			Log.w(TAG, "Caught JSON exception " + e.toString());
			if (responseString != null && responseString.contains("</html>") && responseString.contains("https://www.bahn.de/service/zug/ice-portal")) {
				Log.d(TAG, "Portal does not serve location data, stopping (or not...)");
				// this detection does not seem to work, iceportal responds this for a few requests always
//				stop();
			}
		} catch (Exception e) {
			report(null);
			Log.w(TAG, "Caught exception " + e.toString());
		}
	}
}
