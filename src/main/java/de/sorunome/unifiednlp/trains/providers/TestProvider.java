/*
 * Trains UnifiedNLP
 * Copyright (C) 2022 Sorunome
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.sorunome.unifiednlp.trains;

import android.location.Location;
import android.util.Log;
import java.util.function.Consumer;
import org.microg.nlp.api.LocationHelper;

public class TestProvider extends IntervalProvider {
	public TestProvider(Consumer<Location> report){
		super(report);
	}

	private static final String TAG = TestProvider.class.getName();

	@Override
	protected void callback() {
		try {
			// {"gpsLat":50.162158667,"gpsLng":14.398898333,"speed":90,"delay":null,"altitude":190.40000000000001,"temperature":null}
			Location location = LocationHelper.create("trains");
			location.setLatitude(50.162158667);
			location.setLongitude(14.398898333);
			location.setAccuracy(50.0f);
			Utils.setAltitudeMeters(location, 190);
			Log.d(TAG, "Just reported: " + location);
			report(location);
		} catch (Exception e) {
			Log.w(TAG, "Caught exception", e);
		}
	}
}
