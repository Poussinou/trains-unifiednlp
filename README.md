# Trains UnifiedNLP

This is a UnifiedNLP provider that detects if you are connected to a trains wifi and, if so, provides the location data provided by the train to your android phone.

## Currently supported trains
 - 🇨🇿 České Dráhy (InterJet, Pendolino, some IC)
 - 🇩🇪 Deutsche Bahn (ICE, some IC)
 - 🇭🇺 Magyar Államvasutak
